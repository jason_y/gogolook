FROM python:3.6.0
ADD . /home/mo/circulate
WORKDIR /home/mo/circulate
ENV FLASK_APP autoapp.py
ENV FLASK_DEBUG 1
EXPOSE 5000
RUN pip install --upgrade pip
RUN pip install -r requirements/dev.txt
RUN flask db init
RUN flask db migrate
RUN flask db upgrade
RUN flask run --with-threads